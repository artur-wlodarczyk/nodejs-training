export interface GetReviewsRequestPathParams {
  productId: string;
}

export interface GetReviewsResponse {
  items: ReviewDTO[],
  totalCount: number;
}

export interface ReviewDTO {
  title: string;
  text: string;
}