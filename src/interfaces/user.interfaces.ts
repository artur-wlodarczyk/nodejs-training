
export interface UserQueryParams {
  filter?: string,
  limit?: string,
  offset?: string
}

export namespace UserDTOs {
  export interface User {
    id: string;
    name: string;
  }

}

export interface UserListResponse {
  items: UserDTOs.User[];
  filter?: string;
  limit?: number;
  offset?: number
}

// const Xnspc = (() => {

//   const x = 1
//   const y = 1


//   return {
//     y
//   }
// })()
