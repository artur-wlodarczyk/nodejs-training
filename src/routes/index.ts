import { Router } from "express"
import { addressRoutes } from "./addresses.routes";
import { basketRoutes } from "./basket.routes";
import { orderRoutes } from "./orders.routes";
import { reviewRoutes } from "./reviews.routes";
import { userRoutes } from "./users.routes";
import { wishListRoutes } from "./wishlist.routes";
import { couponsRoutes } from "./coupons.routes";
import { categoryRoutes } from "./categories.routes"
import { paymentsRoutes } from "./payments.routes";
import { settingsRoutes } from "./settings.routes";

const routes = Router()
    .use('/users', userRoutes)
    .use('/basket', basketRoutes)
    .use('/review', reviewRoutes)
    .use('/address', addressRoutes)
    .use('/orders', orderRoutes)
    .use('/payments', paymentsRoutes)
    .use('/wishlist', wishListRoutes)
    .use('/coupons', couponsRoutes)
    .use('/categories', categoryRoutes)
    .use('/settings',settingsRoutes)

export default routes;