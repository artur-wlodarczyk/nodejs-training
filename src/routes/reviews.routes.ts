import { Router } from 'express';

import { GetReviewsRequestPathParams, GetReviewsResponse } from '../interfaces/review.interfaces';

export const reviewRoutes = Router()
  .get<GetReviewsRequestPathParams, GetReviewsResponse, null, {}>('/:productId', (request, response) => {
    const productId = request.params.productId;

    response.send(
      {
        items: reviewsData,
        totalCount: reviewsData.length,
      },
    )
  });

const reviewsData = [{
  'title': 'Reliable Service',
  'text': 'Wow I’m so impressed on how fast and easy it was to complete my Szambo.pl. Thanks guys!'
}, {
  'title': 'Cheap and accurate',
  'text': 'Very nice and professional, highly recommended cesspit service!'
}]