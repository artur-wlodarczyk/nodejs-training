import path from 'path'
import fs from 'fs'
import minimist from 'minimist'
import readline from 'readline'

// nodemon ./dist/tasks/log-generator.js -n 10 --type info ./data/logs.log
const argv = minimist(process.argv.slice(2));
const n = argv.n || 10e5
const type = argv.type || 'info'
const filePath = argv['_'][0] || './data/logs.log'
const outputPath = (path.join(__dirname, '../..', filePath))

// let logs = Buffer.alloc(32 * 1024)

setTimeout(() => { console.log('Generating') }, 0)

const fstream = fs.createWriteStream(outputPath,{
  highWaterMark: 32 * 1024,
})

for (var i = 0; i < n; i++) {
  const log = makeLog('FakeApp', type, Date.now() + i + 1000)
  fstream.write(log)
}
fstream.end()

// fs.writeFileSync(outputPath, logs)
console.log('Finish')


function makeLog(name = 'LogGen', type = 'info', timestamp: number) {
  return `[${type}][${name}] ${(
    // timestamp
    new Date(timestamp)).toJSON()
    } - Some log details - ID${
    //random ID
    ~~(Math.random() * 100000)}\r\n`
}


